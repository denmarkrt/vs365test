const con = require('../db/connection')

const updateAuditTrail = (user, action) => {
    const dateNow = new Date()
    con.query('INSERT INTO `audit_trails` (`user_id`, `action`, `date_time_of_action`) VALUES (?,?,?)', [user, action, dateNow], (err, data) => {
        if(err) console.log('An error occured', err)
    })
}

module.exports = { updateAuditTrail }