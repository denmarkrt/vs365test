const jwt = require('jsonwebtoken');
const secret = 'secret' // We can still make this more secured but for demo purposes, let's just leave it simple for now

const generateAccessToken = (id, username, display_name) => {
	return jwt.sign({id, username, display_name}, secret) 
}

const authenticateToken = (req, res, next) => {
	const authHeader = req.headers['authorization']
	const token = authHeader && authHeader.split(' ')[1]
  
	if (token == null) return res.sendStatus(401)
  
	jwt.verify(token, secret, (err, user) => {
	  console.log(err)
  
	  if(err)
			return res.sendStatus(403)
  
	  req.user = user
	  next()
	})
}

module.exports = { generateAccessToken, authenticateToken }