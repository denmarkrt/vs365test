const express = require('express')
const jwt = require('jsonwebtoken')
const joi = require('joi')
const md5 = require('md5')
const router = express.Router()
const con = require('../db/connection')
const { updateAuditTrail } = require('../helpers/audit')
const { generateAccessToken } = require('../helpers/auth')
const postSchema = joi.object({
	username: joi.string().required(),
	password: joi.string().required()
})

router.post('/', (req, res) => {
	const validate = postSchema.validate(req.body)
	if(validate.error)
		return res.send({success: false})

	const {username, password} = req.body

	con.query('SELECT * FROM `users` WHERE `username`=? AND `password`=?', [username, md5(password)], (err, data) => {
		if(!data.length)
			return res.send({success: false})

		const {id, username, display_name} = data[0]
		const token = generateAccessToken(id, username, display_name)
		updateAuditTrail(id, 'login')

		res.send({
			success: true,
			token
		})
	})
})

module.exports = router