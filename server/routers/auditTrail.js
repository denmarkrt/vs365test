const express = require('express')
const router = express.Router()
const con = require('../db/connection')

router.get('/', (req, res) => {
	con.query('SELECT * FROM `audit_trails` ORDER BY `id` DESC', (err, data) => {
		if(err) console.log('An error occured', err)
		res.send(data)
	})
})

module.exports = router