const express = require('express')
const joi = require('joi')
const router = express.Router()
const con = require('../db/connection')
const { updateAuditTrail } = require('../helpers/audit')
const { authenticateToken } = require('../helpers/auth')

const ipRegexPattern = '^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'
const postSchema = joi.object({
	ip: joi.string().pattern(new RegExp(ipRegexPattern)).required(),
	label: joi.string().required(),
	notes: joi.string().allow('').allow(null)
})
const putSchema = joi.object({
	label: joi.string().required(),
	notes: joi.string().allow('').allow(null)
})

router.get('/', (req, res) => {
	con.query('SELECT * FROM `ips` ORDER BY `id` DESC', (err, data) => {
		if(err) console.log('An error occured', err)
		res.send(data)
	})
})

router.post('/', authenticateToken, (req, res) => {
	const validate = postSchema.validate(req.body)
	if(validate.error)
		return res.send({success: false})

	const {ip, label, notes} = req.body

	con.query('SELECT * FROM `ips` WHERE `ip`=?', [ip], (err, data) => {
		if(err) console.log('An error occured', err)

		if(data.length) {
			return res.send({success: false, reason: 'duplicate_ip'})
		}

		con.query('INSERT INTO `ips` (`ip`, `label`, `notes`) VALUES (?,?,?)', [ip, label, notes], (err, data) => {
			if(err) console.log('An error occured', err)
	
			updateAuditTrail(req.user.id, `added IP with ID ${data.insertId}`)
			res.send({success: true})
		})
	})
})

router.put('/:id', authenticateToken, (req, res) => {
	const validate = putSchema.validate(req.body)
	if(validate.error)
		return res.send({success: false})

	const {id} = req.params
	const {label, notes} = req.body

	con.query('UPDATE `ips` SET `label`=?, `notes`=? WHERE `id`=?', [label, notes, id], (err, data) => {
		if(err) console.log('An error occured', err)

		updateAuditTrail(req.user.id, `updated IP with ID ${id}`)
		res.send({success: true})
	})
})

router.delete('/:id', authenticateToken, (req, res) => {
	const {id} = req.params

	con.query('DELETE FROM `ips` WHERE `id`=?', [id], (err, data) => {
		if(err) console.log('An error occured', err)

		updateAuditTrail(req.user.id, `deleted IP with ID ${id}`)
		res.send({success: true})
	})
})

module.exports = router