require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const con = require('./db/connection')
const port = process.env.SERVER_PORT
const auditTrail = require('./routers/auditTrail')
const auth = require('./routers/auth')
const ip = require('./routers/ip')

app.use(cors())
app.use(bodyParser.json())

app.get('/', (req, res) => {
	res.send('IP Management Server')
})

app.get('/test', (req, res) => {
	con.query('SELECT * FROM `users`', (err,rows) => {
		if(err) throw err;

		console.log('Data received from Db:');
		console.log(rows);
	});
	res.send('OK')
})

app.use('/audit-trail', auditTrail)
app.use('/auth', auth)
app.use('/ip', ip)

app.listen(port, () => {
  console.log(`Listening on port ${port}.`)
})