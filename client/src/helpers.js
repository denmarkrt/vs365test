const isAuthorized = () => {
    const match = document.cookie.match(/^(.*;)?\s*ipmanagement_token\s*=\s*[^;]+(.*)?$/)
    return match === null ? false : true
}

const destroySession = () => {
    document.cookie = "ipmanagement_token= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
    window.location.reload()
}

const getToken = () => {
    const cookieSplit = document.cookie.split('ipmanagement_token=')
    const token = cookieSplit[1].split(';')
    return token[0]
}

module.exports = { isAuthorized, destroySession, getToken }