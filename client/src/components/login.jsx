import React, { useState } from 'react';
import axios from 'axios'
const apiUrl = 'http://127.0.0.1:4000'

function Login() {
	const [username, setUsername] = useState('')
	const [password, setPassword] = useState('')

	const handleLogin = () => {
		if(username === '' || password === '') {
			alert('Please login with complete information.')
			return
		}

		axios.post(apiUrl + '/auth', {username, password})
		.then(response => {
			const { success } = response.data
			
			if(!success) {
				alert('Access denied.')
				clearForm()
				return
			}

			const {token} = response.data
			document.cookie = 'ipmanagement_token=' + token
			window.location.reload()
		})
		.catch(function (error) {
			console.log(error);
		});
	}
	
	const handleUsernameInput = (e) => {
		setUsername(e.target.value)
	}

	const handlePasswordInput = (e) => {
		setPassword(e.target.value)
	}

	const clearForm = () => {
		setUsername('')
		setPassword('')
	}

	return (
	<div className="login-form">
		<h1>Login</h1>
		<input type="text" placeholder="Username" value={username} onChange={handleUsernameInput} />
		<input type="password" placeholder="Password" value={password} onChange={handlePasswordInput} />
		<button onClick={handleLogin}>Login</button>
	</div>
	)
}

export default Login