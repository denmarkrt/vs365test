import React, { useEffect, useState } from 'react'
import axios from 'axios'
const apiUrl = 'http://127.0.0.1:4000'

function AuditTrail() {
	const [auditTrailList, setAuditTrailList] = useState([])

	const generateAuditTrailList = () => {
		axios.get(apiUrl + '/audit-trail')
		.then((response) => {
			setAuditTrailList(response.data)
		})
		.catch((error) => {
			console.log(error);
		})
	}

	const renderAuditTrailList = () => {
		return auditTrailList.map((data, i) => {
			const {user_id, action, date_time_of_action} = data
			return (
				<tr key={i}>
					<td>{user_id}</td>
					<td>{action}</td>
					<td>{date_time_of_action}</td>
				</tr>
			)
		})
	}
	
	useEffect(() => {
		generateAuditTrailList()
	}, [])

	return (
		<div className="table-box audit-trail">
			<h1>Audit Trails</h1>
			<table>
				<thead>
					<tr>
						<th>User ID</th>
						<th>Action</th>
						<th>Date/Time of Action</th>
					</tr>
				</thead>
				<tbody>
					{renderAuditTrailList()}
				</tbody>
			</table>
		</div>
	)
}

export default AuditTrail