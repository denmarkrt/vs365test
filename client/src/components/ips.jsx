import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { getToken } from '../helpers'
const apiUrl = 'http://127.0.0.1:4000'

function IPs() {
	const [ipList, setIpList] = useState([])
	const [id, setId] = useState(0)
	const [ip, setIp] = useState('')
	const [label, setLabel] = useState('')
	const [notes, setNotes] = useState('')
	const [button, setButton] = useState('Add')
	const additionalHeaders = {
		headers: {
			'Authorization': `Bearer ${getToken()}` 
		}
	}

	const generateIpList = () => {
		axios.get(apiUrl + '/ip')
		.then((response) => {
			setIpList(response.data)
		})
		.catch((error) => {
			console.log(error);
		})
	}

	const handleDelete = (id) => {
		alert('Not allowed to delete.')
		return
		// Just keeping the logic here incase needed
		// axios.delete(apiUrl + `/ip/${id}`, additionalHeaders).then((response) => {
		// 	generateIpList()
		// })
		// .catch((error) => {
		// 	console.log(error);
		// })
	}

	const handleEdit = (data) => {
		const {id, ip, label, notes} = data
		setId(id)
		setIp(ip)
		setLabel(label)
		setNotes(notes)
		setButton('Update')
	}

	const handleUpdateIp = () => {
		if(label === '') {
			alert('IP and Label are required fields.')
			return
		}

		axios.put(apiUrl + `/ip/${id}`, {label, notes}, additionalHeaders)
		.then(response => {
			clearForm()
			generateIpList()
		})
		.catch(function (error) {
			console.log(error);
		});
	}

	const renderIpList = () => {
		return ipList.map((data, i) => {
			const {id, ip, label, notes} = data
			return (
				<tr key={i}>
					<td>{ip}</td>
					<td>{label}</td>
					<td>{notes}</td>
					<td>
						<button onClick={()=>handleEdit(data)}>Edit</button>
						<button title="Not allowed to delete" onClick={()=>handleDelete(id)} disabled>Delete</button> {/* Just disabling this one */}
					</td>
				</tr>
			)
		})
	}

	const handleIpInput = (e) => {
		setIp(e.target.value)
	}

	const handleLabelInput = (e) => {
		setLabel(e.target.value)
	}

	const handleNotesInput = (e) => {
		setNotes(e.target.value)
	}
	
	const handleAddIp = () => {
		const ipRegexPattern = /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
		
		if(ip === '' || label === '') {
			alert('IP and Label are required fields.')
			return
		}

		if(ip.match(ipRegexPattern) === null) {
			alert('Please provide a correct IP address.')
			return
		}

		axios.post(apiUrl + '/ip', {ip, label, notes}, additionalHeaders)
		.then(response => {
			if(typeof response.data.reason === 'string' && response.data.reason === 'duplicate_ip') {
				alert('Add failed. Duplication of IP.')
			}

			clearForm()
			generateIpList()
		})
		.catch(function (error) {
			console.log(error);
		});
	}

	const renderForm = () => {
		return (
			<div className="ip-form">
				<input type="text" placeholder="IP*" value={ip} onChange={handleIpInput} disabled={button === 'Update'} />
				<input type="text" placeholder="Label*" value={label} onChange={handleLabelInput} />
				<input type="text" placeholder="Notes" value={notes} onChange={handleNotesInput} />
				<button onClick={button === 'Add' ? handleAddIp : handleUpdateIp}>{button}</button>
				<button onClick={clearForm}>Clear</button>
			</div>
		)
	}

	const clearForm = () => {
		setId(0)
		setIp('')
		setLabel('')
		setNotes('')
		setButton('Add')
	}

	useEffect(() => {
		generateIpList()
	}, [])
	
	return (
		<div className="table-box">
			<h1>IP List</h1>
			{renderForm()}
			<table>
				<thead>
					<tr>
						<th>IP</th>
						<th>Label</th>
						<th>Notes</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{renderIpList()}
				</tbody>
			</table>
		</div>
	)
}

export default IPs