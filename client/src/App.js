import { useState } from "react";
import AuditTrail from "./components/auditTrail";
import Login from "./components/login";
import IPs from "./components/ips"
import { isAuthorized, destroySession } from './helpers'

function App() {
  const [view, setView] = useState('ip')
  const handleLogout = () => {
    destroySession()
  }

  const changeView = () => {
    if(view === 'ip') {
      setView('audit')
    }else {
      setView('ip')
    }
  }

  const renderView = () => {
    if(isAuthorized()) {
      return (
        <>
          <button onClick={handleLogout}>Logout</button>
          <button onClick={changeView}>{view === 'ip' ? 'Audit Trails' : 'IP List'}</button>
          {view === 'ip' && <IPs />}
          {view === 'audit' && <AuditTrail />}
        </>
      )
    }

    return <Login />
  }

  return (
    <div className="App">
      {renderView()}
    </div>
  );
}

export default App;
