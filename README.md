# IP Adress Management Solution

## About the Directories
### ./client
- Contains the frontend codebase
- Uses ReactJs Javascript library
### ./db
- Contains SQL file to be imported on the database
- Already contains one user to be used when checking the app
### ./server
- Contains the serverside/backend codebase
- Uses ExpressJs Javascript framework
- Contains RESTful APIs

## How to Run the Project
In order to get started with everything, you must clone this repository first.

### Database
- Run any MySQL database client that you have and create a database named `ip_management`
- Once created, select that database and import the `ip_management.sql` file under the `./db` folder
- By default, you will have three tables created (namely: `audit_trails`, `ips`, `users`) and one row inserted in `users` table

### Server
- Open a command line
- Navigate inside `./server` folder
- For Windows users, run `build.bat`
- For Linux users, run `bash build.sh`
- At this point, the server should be running on port `4000`

> If you are having trouble connecting to the database, you can terminate the server first and modify the database credentials within the `.env` file. \
> Take note that the `SERVER_PORT` value should stay as `4000`. \
> You can run the server again by executing `npm start` command.

### Client
- Open another separate command line
- Navigate inside `./client` folder
- For Windows users, run `build.bat`
- For Linux users, run `bash build.sh`
- After all of the steps above, you should be able to access `http://127.0.0.1:3000` or `http://localhost:3000`

> Note: If port `3000` is already used, the command line might prompt and ask you if you want to continue to a different port. Just accept it and it should not cause any issue at all.

### Logging In
- You can login to the application using this:
  - Username: admin
  - Password: adminuser

## Final Words
If anything goes wrong or if there are some things that needs to be clarified, please feel free to reach me out via email at [denmarkrt@gmail.com](mailto:denmarkrt@gmail.com).